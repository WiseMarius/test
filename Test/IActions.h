#pragma once
class Card;

class IActions
{
public:
	virtual void F1() = 0;											// Find three card, trigger dealing
	virtual bool F2(int , int, int) = 0;		// Check rules for cards

	IActions();
	virtual ~IActions();
};

