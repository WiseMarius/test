#include "DealingSystem.h"
#include <iostream>
#include "Constants.h"


DealingSystem::DealingSystem()
{
	std::cout << "Initializing board: " << std::endl;

	for (auto index = 0; index < GAME_INITIAL_CARDS; index++)
	{
		this->m_gameBoard.addCard(this->m_deck.getCard());
	}
}


DealingSystem::~DealingSystem()
{
}

void DealingSystem::F1()
{
	auto checkCards = false;

	for (auto indexCard1 = 0; indexCard1 < this->m_gameBoard.getCardsOnTable().size(); indexCard1++)
	{
		for (auto indexCard2 = indexCard1 + 1; indexCard2 < this->m_gameBoard.getCardsOnTable().size(); indexCard2++)
		{
			for (auto indexCard3 = indexCard2 + 1; indexCard3 < this->m_gameBoard.getCardsOnTable().size(); indexCard3++)
			{
				this->F2(indexCard1, indexCard2, indexCard3);
			}
		}
	}
}

bool DealingSystem::F2(int indexCard1, int indexCard2,int indexCard3)
{
	auto figure1 = this->m_gameBoard.getCardsOnTable().at(indexCard1).getId();
	auto figure2 = this->m_gameBoard.getCardsOnTable().at(indexCard2).getId();
	auto figure3 = this->m_gameBoard.getCardsOnTable().at(indexCard3).getId();

	if (figure1 / 100 != ((figure2 / 10) % 10) != figure3 % 10)
	{
		this->m_gameBoard.removeCard(indexCard1);

		return true;
	}

	return false;
}

void DealingSystem::startGame()
{
	std::cout << "Starting game: " << std::endl;
	this->m_deck.shuffle();
	std::cout << "Deck shuffled" << std::endl;
}
