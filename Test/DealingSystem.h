#pragma once
#include "Deck.h"
#include "GameBoard.h"
#include "IActions.h"

class DealingSystem : public IActions
{
private:
	GameBoard m_gameBoard;
	Deck m_deck;

public:
	DealingSystem();
	~DealingSystem();

	void F1() override;
	bool F2(int, int, int) override;

	void startGame();
};

