#pragma once
#include <vector>
#include "Card.h"

class Deck
{
public:
	Deck();
	~Deck();
	void shuffle();
	void print();

	Card getCard();
private:
	std::vector<Card> m_deck;
};

