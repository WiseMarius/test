#include "Deck.h"
#include "Card.h"
#include "Constants.h"
#include <iostream>


Deck::Deck()
{
	for (auto numberIndex = 0; numberIndex < CARD_NUMBER_TYPES; numberIndex++)
	{
		for (auto symbolIndex = 0; symbolIndex < CARD_SYMBOL_TYPES; symbolIndex++)
		{
			for(auto shadingIndex = 0; shadingIndex < CARD_SHADING_TYPES; shadingIndex++)
			{
				for(auto colorIndex = 0; colorIndex < CARD_COLOR_TYPES; colorIndex++)
				{
					auto currentCard = Card(static_cast<Card::Number>(numberIndex + 1),
						static_cast<Card::Symbol>(symbolIndex + 1),
						static_cast<Card::Shading>(shadingIndex + 1),
						static_cast<Card::Color>(colorIndex + 1));

					this->m_deck.push_back(currentCard);
				}
			}
		}
	}
}


Deck::~Deck()
{
}

void Deck::shuffle()
{
	//foreach card from the deck
	for (auto &currentDeck : this->m_deck)
	{
		//pick a random card
		auto swapIndex = dis(gen);

		//swap it with the current card
		std::swap(currentDeck, m_deck[swapIndex]);
	}
}

void Deck::print()
{
	for (auto card : this->m_deck)
	{
		std::cout << card.getId() << ", ";
	}

	std::cout << std::endl;
}

Card Deck::getCard()
{
	auto card = this->m_deck.at(this->m_deck.size() - 1);

	this->m_deck.pop_back();

	return card;
}
