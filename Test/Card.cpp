#include "Card.h"



Card::Card(): m_number(), m_symbol(), m_shading(), m_color()
{
}

Card::Card(Number number, Symbol symbol, Shading shading, Color color)
	: m_number(number),
	m_symbol(symbol),
	m_shading(shading),
	m_color(color)
{
}

unsigned Card::getId() const
{
	auto id = static_cast<int>(this->m_number) * 1000;
	id += static_cast<int>(this->m_symbol) * 100;
	id += static_cast<int>(this->m_shading) * 10;
	id += static_cast<int>(this->m_color);

	return id;
}



