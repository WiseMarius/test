#include "GameBoard.h"
#include "Constants.h"


std::vector<Card> GameBoard::getCardsOnTable() const
{
	return m_cardsOnTable;
}

GameBoard::GameBoard()
{

}


GameBoard::~GameBoard()
{
}

void GameBoard::addCard(const Card& pCard)
{
	this->m_cardsOnTable.push_back(pCard);
}

void GameBoard::removeCard(int index)
{
	auto iterator = this->m_cardsOnTable.begin() + index;

	this->m_cardsOnTable.erase(iterator);
}
