#pragma once

class Card
{
public:

	enum class Number
	{
		ONE = 1,
		TWO,
		THREE
	};

	enum class Symbol
	{
		DIAMOND = 1,
		SQUIGGLE,
		OVAL
	};

	enum class Shading
	{
		SOLID = 1,
		STRIPED,
		OPEN
	};

	enum class Color
	{
		RED = 1,
		GREEN,
		BLUE
	};

private:
	Number m_number;
	Symbol m_symbol;
	Shading m_shading;
	Color m_color;

public:
	Card();
	Card(Number number, Symbol symbol, Shading shading, Color color);

	unsigned int getId() const;
};