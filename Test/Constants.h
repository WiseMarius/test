#pragma once

//Given names
#define DECK_NAME	Francois
#define BOARD_NAME	Gerhart
#define DEALER_NAME Johnny
#include <random>

//Card types
static const int CARD_NUMBER_TYPES	 = 3;
static const int CARD_SYMBOL_TYPES	 = 3;
static const int CARD_SHADING_TYPES	 = 3; 
static const int CARD_COLOR_TYPES	 = 3;

//For random
static std::random_device rd;
static std::mt19937 gen(rd());
static std::uniform_int_distribution<>dis(0, 80);

//Game rules
static const int GAME_INITIAL_CARDS = 12;
