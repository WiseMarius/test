#pragma once
#include <vector>
#include "Card.h"

class GameBoard
{
private:
	std::vector<Card> m_cardsOnTable;

public:
	std::vector<Card> getCardsOnTable() const;


	GameBoard();
	~GameBoard();

	void addCard(const Card &);
	void removeCard(int index);
};

